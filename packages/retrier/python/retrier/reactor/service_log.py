import datetime
import enum
from typing import Optional, Union

import ncs
from ..common import utils


class Level(enum.Enum):
    ALL = 1
    TRACE = 2
    DEBUG = 3
    INFO = 4
    WARN = 5
    ERROR = 6


def log(service: ncs.maagic.Node, entry_type: str, level: Union[Level, str],
        message: str, when: Optional[Union[datetime.datetime, str]] = None):
    """Add an entry to $service/log/log-entry list (persistent oper CDB)

    :param service: service maagic node
    :param entry_type: NCS log entry type (identity)
    :param level: NCS log entry level (enum [all, trace, debug, info, warn, error])
    :param message: log message
    :param when: None, datetime.datetime or a correctly formatted string for yang:date-and-time type. When None, utcnow() will be used.
    """
    if when is None or isinstance(when, datetime.datetime):
        log_entry = service.log.log_entry.create(utils.format_yang_date_and_time(when))
    else:
        log_entry = service.log.log_entry.create(when)
    log_entry.type = entry_type
    if isinstance(level, Level):
        log_entry.level = level.name.lower()
    else:
        log_entry.level = level
    log_entry.message = message

    # prune old entries. Normally this would be done by NCS, but doesn't seem to work
    # for entries created by user code
    trans = ncs.maagic.get_trans(service)
    max_size = int(trans.get_elem('/services/logging/max-size'))
    log_entry_keys = service.log.log_entry.keys()
    if len(log_entry_keys) > max_size:
        # take len(log_entry_keys) - max_size entries from beginning of list
        for log_entry in log_entry_keys[:len(log_entry_keys) - max_size]:
            del service.log.log_entry[log_entry]
