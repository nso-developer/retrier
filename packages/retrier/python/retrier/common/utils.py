import re
from datetime import datetime
from typing import Optional

import ncs


def format_yang_date_and_time(timestamp: Optional[datetime]=None, microseconds=True) -> str:
    """Format a timestamp in yang:date-and-time format

    :param timestamp: optional datetime instance. Defaults to utcnow()
    :param microseconds: whether to include microseconds in the timestamp. Defaults to True
    :return: yang:date-and-time formatted string"""
    if not timestamp:
        timestamp = datetime.utcnow()
    elif not isinstance(timestamp, datetime):
        raise ValueError('timestamp must be datetime instance')
    if microseconds:
        fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
    else:
        fmt = '%Y-%m-%dT%H:%M:%SZ'
    return timestamp.strftime(fmt)


def convert_yang_date_and_time_to_datetime(yang_date_and_time):
    """Convert a timestamp stored in a leaf of type yang:date-and-time to python datetime"""
    if not yang_date_and_time:
        return None
    # the timestamps are read as 2018-02-20T13:15:14+00:00
    # strip the extra +00:00 from the end
    yang_date_and_time = re.sub(r'[+-]\d{2}:\d{2}$', '', yang_date_and_time)
    if re.search(r'\.\d+$', yang_date_and_time):
        fmt = '%Y-%m-%dT%H:%M:%S.%f'
    else:
        fmt = '%Y-%m-%dT%H:%M:%S'
    return datetime.strptime(yang_date_and_time, fmt)


class Nonexistent(Exception):
    pass


def maagic_safe_get_node(root_or_trans, kp):
    try:
        return ncs.maagic.get_node(root_or_trans, kp)
    except KeyError:
        raise Nonexistent(kp)
