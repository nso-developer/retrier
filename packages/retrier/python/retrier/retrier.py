from collections import defaultdict
import datetime
import logging
import math
import re
import time
import traceback
from typing import DefaultDict, List, Optional, Set, Tuple

import ncs
from alarm_sink import alarm_sink
from bgworker import background_process
from maagic_copy.maagic_copy import path_to_xpath
from ncs.experimental import Query

from .common import utils
from .reactor import service_log


def run_retrier():
    log = logging.getLogger('service-retrier')
    retried_zombies: DefaultDict[str, float] = defaultdict(float)
    while True:
        with ncs.maapi.single_read_trans('python-retrier', 'system') as t:
            settings = ncs.maagic.get_node(t, '/retrier:retrier')
            retry_zombies = settings.retry_zombies
            retry_zombies_grace_period = settings.zombies_grace_period
            disabled_services = [str(instance) for instance in settings.disabled_service_instances]
            log.debug(f'Service retrier disabled for the following instances: {disabled_services}')
        retriable_services, unretriable_services = find_retriable_services(log, disabled_services)
        for service in retriable_services:
            try:
                retry_service(service, log, False)
            except utils.Nonexistent:
                pass
            except Exception:
                log.error(f'Error retrying service {service} with exception: {traceback.format_exc()}')

        handle_unretriable_alarms(unretriable_services)

        # Only retry zombies in NSO 5.7 and later with special code
        # below.
        # In previous version of NSO, zombies can be adding
        # /zombies//plan/failed nodes to retry-service list.
        if retry_zombies and ncs.LIB_VSN_STR >= '07070000':
            retriable_zombies = find_retriable_zombies(log)
            for zombie in retriable_zombies:
                last_retry = retried_zombies[zombie]
                current_time = time.time()
                if last_retry == 0:
                    last_retry = current_time
                    retried_zombies[zombie] = current_time
                if current_time - last_retry > retry_zombies_grace_period:
                    log.debug(f'Retrying: zombie service {zombie} last retried {current_time - last_retry} seconds ago')
                    try:
                        retry_service(zombie, log, True)
                    except utils.Nonexistent:
                        pass
                    except Exception:
                        log.error(f'Error retrying zombie {zombie} with exception: {traceback.format_exc()}')
                    retried_zombies[zombie] = current_time
                else:
                    log.debug(f'Skip: zombie service {zombie} last retried {current_time - last_retry} seconds ago')
            # clean up previous list
            for old_z in list(retried_zombies.keys()):
                if old_z not in retriable_zombies:
                    log.debug(f'zombie service {old_z} disappeared: last retried {current_time - retried_zombies[old_z]} seconds ago')
                    del retried_zombies[old_z]
        else:
            # reset
            retried_zombies = defaultdict(float)

        time.sleep(10)

def handle_unretriable_alarms(unretriable_services: Set[str]):
    with ncs.maapi.single_read_trans('python-retrier-oper', 'system', db=ncs.OPERATIONAL) as t:
        ask = alarm_sink.AlarmSink(t.maapi)
        for service_model_xpath in unretriable_services:
            alarm_id = alarm_sink.AlarmId('ncs', service_model_xpath,
                                          type='retrier:missing-service-retrier-container',
                                          specific_problem=None)
            alarm = alarm_sink.Alarm(alarm_id, severity=alarm_sink.PerceivedSeverity.WARNING,
                                     alarm_text="No 'service-retrier' container found in service model", impacted_objects=None)
            ask.submit_alarm(alarm)

        to_clear: Set[alarm_sink.AlarmId] = set()
        with Query(t, "alarm[type='retrier:missing-service-retrier-container']",
                   context_node='/al:alarms/alarm-list',
                   select=['device', 'managed-object', 'type', 'specific-problem'],
                   result_as=ncs.QUERY_STRING) as q:
            for found_alarm_id in q:
                if found_alarm_id[1] not in unretriable_services:
                    to_clear.add(alarm_sink.AlarmId(*found_alarm_id))

        for alarm_id in to_clear:
            alarm = alarm_sink.Alarm(alarm_id, severity=alarm_sink.PerceivedSeverity.WARNING,
                                     alarm_text='Service instances no longer in failed state')
            alarm.cleared = True
            ask.submit_alarm(alarm)


def retry_service(retriable_service: str, log: logging.Logger, is_zombie: bool) -> Tuple[bool, Optional[str]]:
    with ncs.maapi.Maapi() as maapi:
        with ncs.maapi.Session(maapi, f'python-service-retrier-{retriable_service}/re-deploy', 'system'):
            log.info(f'Retrying service {retriable_service} with action re-deploy')
            retry_start = datetime.datetime.utcnow()
            with maapi.start_read_trans(db=ncs.OPERATIONAL) as oper_t:
                service = utils.maagic_safe_get_node(oper_t, retriable_service)
                service_xpath = path_to_xpath(service)

                try:
                    # NCS actions (like re-deploy) do not have an output model. The error is
                    # relayed to us by an _ncs.error.Error.
                    service.re_deploy()
                    success, message = True, None
                except Exception as e:
                    log.error(f'Error retrying {retriable_service} with action re-deploy: {e}')
                    success, message = False, str(e)

            # do not write to zombies
            if is_zombie:
                return success, message

            retry_end = datetime.datetime.utcnow()
            retry_duration = retry_end - retry_start

            # log the retry attempt to the service being retried's /log container
            # and clear the retrier disabled flag.
            with maapi.start_write_trans(db=ncs.OPERATIONAL) as t:
                write_service = utils.maagic_safe_get_node(t, service)
                raise_alarm = False
                max_backoff = utils.maagic_safe_get_node(t, '/retrier:retrier/failure-detection/max-backoff')
                if success:
                    # remove the disabled-until and backoff-period
                    write_service.service_retrier.delete()
                    log_message = f'retried action re-deploy in {retry_duration}'
                    log.info(f'{retriable_service}: {log_message}')
                else:
                    # calculate the exponential backoff (up to max-backoff)
                    backoff = write_service.service_retrier.backoff_period or \
                            math.ceil(retry_duration.total_seconds() / 60)
                    backoff = min(2*backoff, max_backoff)

                    error_transient = is_error_transient(message)
                    if not error_transient:
                        # for non-transient errors (NED issues, config errors), check if we
                        # have seen the error message before. if so, immediately set backoff
                        # to max-backoff and raise an alarm

                        # we could preprocess the message here to perform a fuzzy match?
                        if message == write_service.service_retrier.previous_message:
                            backoff = max_backoff
                            raise_alarm = True
                        write_service.service_retrier.previous_message = message

                    err_message = ', error ' + message if message else ''
                    loop_message = ' (possible loop detected)' if raise_alarm else ''
                    log_message = f'failed to retry action re-deploy in {retry_duration}{err_message}. Transient: {error_transient}{loop_message} Backoff: {datetime.timedelta(minutes=backoff)}'

                    write_service.service_retrier.backoff_period = backoff
                    disabled_until = retry_end + datetime.timedelta(minutes=backoff)
                    write_service.service_retrier.disabled_until = utils.format_yang_date_and_time(disabled_until)

                    log.warning(f'{retriable_service}: {log_message}')
                service_log.log(write_service,
                                entry_type='service-modified',
                                level='info' if success else 'error',
                                message=log_message,
                                when=utils.format_yang_date_and_time(retry_start))
                t.apply()

            ask = alarm_sink.AlarmSink(maapi)
            # Is 'ncs' a good choice for the alarmed device?!
            # In some cases, the service instance does touch a single device, but in others
            # it may be responsible for multiple.
            # The new IETF YANG Alarm model will get rid of the device in favor of resource,
            # so this will be moot anyway
            alarm_id = alarm_sink.AlarmId('ncs', service_xpath,
                                          type='retrier:service-retrier-loop',
                                          specific_problem=None)
            alarm = alarm_sink.Alarm(alarm_id, severity=alarm_sink.PerceivedSeverity.WARNING,
                                     alarm_text=None, impacted_objects=[retriable_service])
            if raise_alarm:
                alarm.alarm_text = f'Loop detected: {message}'
            else:
                alarm.alarm_text = 'Service retried successfuly.'
                alarm.cleared = True
            ask.submit_alarm(alarm)

            return success, message

def find_retriable_zombies(log: logging.Logger) -> Set[str]:
    zombies: Set[str] = set()
    with ncs.maapi.single_read_trans('python-retrier', 'system', db=ncs.OPERATIONAL) as t:
        root = ncs.maagic.get_root(t)
        for service in root.zombies.service:
            if not service.pending_delete:
                zombies.add(service._path)
            else:
                log.debug(f'Skipping zombie service {service.service_path}, pending-delete is set')
    return zombies


def find_retriable_services(log: logging.Logger, disabled_services: List[str]) -> Tuple[Set[str], Set[str]]:
    """Finds service instances that match the configured XPath filter(s)

    A retriable service is one that matches an XPath filter *and* it contains
    the 'service-retrier' container for storing operational state. Returns the
    list of keypaths for retriable services and matches without the necessary
    container.
    """
    retriable_services: Set[str] = set()
    unretriable_services: Set[str] = set()

    with ncs.maapi.single_read_trans('python-retrier', 'system', db=ncs.OPERATIONAL) as t:
        def _find_retriable_services(kp, value_unused):
            # The XPath filter finds .../plan/failed leaves, the service
            # instance list entry is two levels up
            service_kp = str(kp[2:])
            service = utils.maagic_safe_get_node(t, service_kp)
            # check if service retrier was disabled for this service
            try:
                disabled_until = utils.convert_yang_date_and_time_to_datetime(service.service_retrier.disabled_until)
            except AttributeError:
                log.warn(f'{service._path}: service was listed as interesting, but is missing "service-retrier" container')
                # strip the keys off service list entry path, leaving just the path to service list
                service_model_kp = str(kp[3:])
                unretriable_services.add(path_to_xpath(service_model_kp))
                return
            if disabled_until is not None and disabled_until > datetime.datetime.utcnow():
                log.info(f'{service._path}: service retrier disabled until {disabled_until}')
                return

            if str(kp[2:]) not in disabled_services:
                retriable_services.add(service_kp)

        root = ncs.maagic.get_root(t)
        for service_path in root.retrier.retry_service:
            t.xpath_eval(service_path, _find_retriable_services, trace=None, path='')

    return retriable_services, unretriable_services


def is_error_transient(message: Optional[str]) -> bool:
    """Based on the retry action and error message, figure out if the failure can be fixed by retrying.

    Right now, this is a very simple blacklist, filtering out known error messages
    that are the result of device errors / NED problems.
    """
    if not message:
        return True

    if 'External error in the NED implementation' in message:
        return False
    elif 'RPC error towards' in message:
        return False
    elif 'No working initial credentials found' in message:
        return False
    elif re.search(r'^[^ ]*: invalid value', message, re.IGNORECASE):
        return False
    return True


class RetrierApp(ncs.application.Application):
    p: background_process.Process

    def setup(self):
        self.p = background_process.Process(self, run_retrier, config_path='/retrier:retrier/enabled', backoff_timer=10)
        self.p.start()

    def teardown(self):
        self.p.stop()
