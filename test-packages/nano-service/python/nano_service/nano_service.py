# -*- mode: python; python-indent: 4 -*-
import ncs

# ------------------------
# SERVICE CALLBACK EXAMPLE
# ------------------------
class NanoServiceConfigured(ncs.application.NanoService):
    @ncs.application.NanoService.create
    def cb_nano_create(self, tctx, root, service, plan, component, state, proplist, component_proplist):
        self.log.debug("NanoService create ", state)
        if root.service_switch.fail:
            root.foo = 'bar' # this will fail


# ---------------------------------------------
# COMPONENT THREAD THAT WILL BE STARTED BY NCS.
# ---------------------------------------------
class NanoService(ncs.application.Application):
    def setup(self):
        self.log.info('NanoService RUNNING')
        self.register_nano_service('nano-service-servicepoint', 'nano-service:nano-service', 'nano-service:configured', NanoServiceConfigured)

    def teardown(self):
        self.log.info('NanoService FINISHED')
